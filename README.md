# UCSB Neuroscience and Neuroengineering Lab
![Bot Chassis](src/images/Bot-Assy-3 v15.png)

[SLSLAB](http://slslab.org) Telepresence Robot

This is the 2nd iteration of the robot developed [here](https://www.embeddedsolutions.design/project-2).

# Project Structure

This repo includes all of the code required to run the entire system.

All of the firmware, scripts required to flash the motor controller, and the video stream control functions are found in the folder "motion_controller".

All remote PID tuning scripts are found in "PID_Tuning."

All the code for reading the trackball data, translating that data into encoder counts, and transmitting to the robot are found in the folder "trackball".

Some helper scripts for remote debugging are found in "shell_commands".

The main GUI application is found in "main.py".

# Robot Motion Operation

The firmware from the board is updatable from anywhere in the world (updating from Seychelles to UCSB).

* A remote desktop runs PlatformIO & MBed to compile and build any firmware changes. 
* These compiled files are pushed to the Jetson Nano board via RSync.
* The Jetson Nano uploads the new code to the 32 bit controller via OpenOCD & GDB

During runtime, the robot accepts motion commands:

* A task manager program (written in Python & NodeJS) verifies that all essential scripts are up and running.
* A TCP socket reads and verifies all incoming motion coordinates
* A python script converts these changes in robot position to motor encoder counts
* Changes in motor position is transmitted over serial to 32-bit arm controller



# Remote Servo Tuning

## Click the picture below to see the YouTube video.

[![Remote Motor Tuning](http://img.youtube.com/vi/m8hOdliGSg8/0.jpg)](https://youtu.be/m8hOdliGSg8 "Remote Motor Tuning")

Here is a brief video showing how remote firmware updates and motor tuning work. 

The tool chain is the same as above, only the network socket has been setup to to accept PID tuning parameters that can be written to from a python script running on the desktop.

Calibration begins at 2:20.

# Improvements - Field of View: Camera Calibration & Correction

Mice have a horizontal field-of-view (FOV) of nearly 200 degrees, and a vertical FOV close to 20 degrees.

The first robot used the raspberry pi camera, which has a horizontal FOV of ~53 degrees and 41 vertical.

Using a wide-angle lens with image correction, we able to get a FOV that is much closer to 100 degrees.

##### Rectilinear Image Correction

![Image Correction](src/images/image_correction.png)

##### Correction Setup

##### ![Correction Setup](src/images/correction-setup.png)

# Improvements - Video Resolution & Latency

Minimum possible video stream latency and video synchronization was a huge requirement for this project, as the telepresence robot needs to be easily correlated with the microscope data, and a slow video stream will greatly alter the mice's mental process.

#####  720p at 120FPS with ~66 ms +/- 8 ms latency. Click the picture below to see the YouTube video.

[![Video Latency](http://img.youtube.com/vi/6VKGF9i7AgA/0.jpg)](http://www.youtube.com/watch?v=6VKGF9i7AgA "Video Stream Latency")

*This is about 30 ms faster than our previous iteration, all while transporting about 3x the pixel data!*

*To execute this experiment, I built a binary clock using LEDs and filmed it using an I-Phone's slow motion camera feature. This camera captures with an 8mS period. So the binary clocks fastest LED oscillates on the same frequency. Therefore, all results are +/- 8 mS.*

### Codec Testing

To get the best result, I permeated through all of the best available video codecs - there is a lot of jargon associated with these codecs, but I will try to explain the results from the top down. Firstly, there are two different, ubiquitous libraries for encoding video streams via Linux:

* OpenMAX - referred to as OMX

* V4L - Video 4 Linux

The Nvidia board has special hardware for executing video encoding, and has support for both libraries. So all video codec options have been tested using both encoder drivers.

Secondly, the Nvidia board also has optimized hardware support for a handful of video codecs, most relevantly:

* H264

* H265

* VP8 

Lastly, there are a variety of network data frames that can be used to transport the encoded data. I tested:

* Real-Time Transport Protocol - RTP

* MPEG Transport Stream - MPEGTS

I tested all possible permutations of encoding drivers, codecs, and transport protocols to make sure we arrived at the best solution.  All units below are in mS.

#### OpenMAX Encoding

```markdown
| Codec\Container |  RTSP  | MPEGTS |
|:---------------:|:------:|:------:|
| H264            |  76.00 | 130.40 |
| H265            | 144.44 | 185.33 |
| VP8             | 110.00 | N.A    |
```

#### Video4Linux

```markdown
| Codec\Container |  RTSP  | MPEGTS |
|:---------------:|:------:|:------:|
| H264            | 155.20 | 155.20 |
| H265            | 135.00 | 194.67 |
| VP8             | 108.00 | N.A.   |
```

### Codec Interlatency

This will time how long it takes to read the camera, encode the video, and buffer it for network transfer. These graphs plot the total time for individual stream element to output a frame since the frames creation. So the graph is cumulative with the highest line representing the total time of required for hardware encoding/decoding.

##### Robot: Video Encoding

![Robot Video Stream Interlatency](src/images/interlatency-robot.png)

You can see that on average it takes about 6 mS for the robot to read, encode, and buffer the video stream for network transfer. There is some kind of systemic problem that brings the time up to 10mS. This may be addressable at a future date.

##### Desktop : Video Stream Decoding:

![Desktop Video Stream Interlatency](src/images/interlatency-desktop.png)

If you ignore the startup lag, you can see that it takes the desktop computer about 5 mS to capture, decode, and send the video to the display.

This means that with our 76 mS latency, only about 16 mS of that is actually spent encoding and decoding hardware. This means that the majority of our latency is created at a network communication level, not a video encoding and decoding level.

### Frame Rate

The next biggest improvement came from selecting frame rate. Because H264 transmits changes in frames,  streaming at a higher framerate creates more opportunities for the compression algorithm to find a change and incorporate it into the video stream.

This is counter-intuitive, because a higher frame rate creates significantly more data to successfully transmit. For this reason, I directly measured the network load using wireshark to ensure no data is dropped.

##### 5 Frames Per Second

![5 FPS](src/images/fps5.png)

##### 120 Frames Per Second

![120 FPS](src/images/fps120.png)

You can see that the network still has down time between video frames.

# Improvements - Trackball

The first robot had a separate raspberry pi with two USB mice capturing trackball movement and transmitting this data with an XBee. In the second iteration of the robot, the USB mice connect directly to the desktop and transmits the data to the robot over a TCP socket.

This really simplifies the development cycle, and does not introduce noticeable latency in the robots motion.

# Improvements - Task Management

This first GUI used to remote login to the robot to start and stop tasks and threads. Needless to say, this was not the most stable way to run the system. The new program sets up a RESTful API to display the status of the robot and allow the desktop GUI to send commands.

There are far fewer necro-threads this way.

# REQUIRED: gst-absolutetimestamps
build from here: https://github.com/waymond91/gst-absolutetimestamps